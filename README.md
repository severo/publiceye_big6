# README

- [`original_data`](./original_data/): the original files
- [`prepared_data`](./prepared_data/): data prepared in order to do the analysis. See the detail of [how they have been prepared](prepared_data/README.md).
