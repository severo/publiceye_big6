# JS scripts

- [`7193f10baad90950.js`](./7193f10baad90950.js): downloaded from https://api.observablehq.com/d/7193f10baad90950.js. Takes the prepared data (you must manually load the data with the buttons), joins the sales with the PAN list, compute the PAN weight, sum the sales and volume, export the CSV
