// URL: https://observablehq.com/d/7193f10baad90950
// Title: Sales - Big 6
// Author: Sylvain Lesage (@severo)
// Version: 350
// Runtime version: 1

const m0 = {
  id: "7193f10baad90950@350",
  variables: [
    {
      inputs: ["md"],
      value: (function(md){return(
md`# Sales - Big 6

## Load data
`
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`
### PAN list
Load the pan_pesticides.csv file:`
)})
    },
    {
      name: "viewof panFile",
      inputs: ["html"],
      value: (function(html){return(
html`<input type=file accept="text/*">`
)})
    },
    {
      name: "panFile",
      inputs: ["Generators","viewof panFile"],
      value: (G, _) => G.input(_)
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`List of 310 dangerous pesticides (PAN list):`
)})
    },
    {
      name: "pan",
      inputs: ["Files","panFile","d3"],
      value: (function(Files,panFile,d3){return(
Files.text(panFile)
  .then(d3.csvParse)
  .then(rows =>
    rows.map(row => {
      row.uppername = row.name.toLocaleUpperCase("en-US");
      return row;
    })
  )
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`### Sales
Load the sales.csv file:
`
)})
    },
    {
      name: "viewof salesFile",
      inputs: ["html"],
      value: (function(html){return(
html`<input type=file accept="text/*">`
)})
    },
    {
      name: "salesFile",
      inputs: ["Generators","viewof salesFile"],
      value: (G, _) => G.input(_)
    },
    {
      inputs: ["sales"],
      value: (function(sales){return(
sales.filter(sale => sale.manufacturers.length > 3)
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`List of 19.206 sales in 2017 (from the 29.783 sales in 2016-2017):`
)})
    },
    {
      name: "sales",
      inputs: ["Files","salesFile","d3"],
      value: (function(Files,salesFile,d3){return(
Files.text(salesFile)
  .then(d3.csvParse)
  .then(rows =>
    rows
      .filter(row => row.year === "2017")
      .map(row => {
        row.compounds = row.ai.split("/").map(str => str.trim());
        row.manufacturers = row.manufacturer.split("/").map(str => str.trim());
        return row;
      })
  )
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`## Coefficient by proportion of PAN compounds in products of sales`
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`TODO: manually review all the compounds, from the PAN list and the sales list, to see if there are missing checks`
)})
    },
    {
      name: "salesWithPanCoeff",
      inputs: ["sales","panLut"],
      value: (function(sales,panLut){return(
sales.map(sale => {
  if (sale.compounds.length > 0) {
    sale.panCoeff =
      sale.compounds.filter(com => com in panLut).length /
      sale.compounds.length;
  } else {
    sale.panCoeff = 0;
  }
  return sale;
})
)})
    },
    {
      inputs: ["salesWithPanCoeff"],
      value: (function(salesWithPanCoeff){return(
salesWithPanCoeff.filter(sale => sale.panCoeff > 0 && sale.compounds.length > 4)
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`TODO: use the .manufacturers field to find the associations, and fix the sums`
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`## Prepared objects`
)})
    },
    {
      name: "salesByManufacturer",
      inputs: ["salesWithPanCoeff"],
      value: (function(salesWithPanCoeff){return(
salesWithPanCoeff.reduce((acc, cur) => {
  if (!(cur.manufacturer in acc)) {
    acc[cur.manufacturer] = [];
  }
  acc[cur.manufacturer].push(cur);
  return acc;
}, {})
)})
    },
    {
      name: "manufacturers",
      inputs: ["salesByManufacturer"],
      value: (function(salesByManufacturer){return(
Object.keys(salesByManufacturer)
)})
    },
    {
      name: "salesByCountry",
      inputs: ["salesWithPanCoeff"],
      value: (function(salesWithPanCoeff){return(
salesWithPanCoeff.reduce((acc, cur) => {
  if (!(cur.country in acc)) {
    acc[cur.country] = [];
  }
  acc[cur.country].push(cur);
  return acc;
}, {})
)})
    },
    {
      name: "countries",
      inputs: ["salesByCountry"],
      value: (function(salesByCountry){return(
Object.keys(salesByCountry)
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`## Sales and volumes`
)})
    },
    {
      name: "getTotalFromColumn",
      inputs: ["getTotalFromColumnWithPanCoeff"],
      value: (function(getTotalFromColumnWithPanCoeff){return(
function getTotalFromColumn(arr, col) {
  //return getTotalFromColumnWithoutCoeff(arr, col);
  return getTotalFromColumnWithPanCoeff(arr, col);
}
)})
    },
    {
      name: "getTotalFromColumnWithoutCoeff",
      value: (function(){return(
function getTotalFromColumnWithoutCoeff(arr, col) {
  return arr.reduce((acc, cur) => {
    if (cur[col] && !isNaN(cur[col])) {
      acc += +cur[col];
    }
    return acc;
  }, 0);
}
)})
    },
    {
      name: "getTotalFromColumnWithPanCoeff",
      value: (function(){return(
function getTotalFromColumnWithPanCoeff(arr, col) {
  return arr.reduce((acc, cur) => {
    if (cur[col] && !isNaN(cur[col])) {
      acc += +cur[col] * cur.panCoeff;
    }
    return acc;
  }, 0);
}
)})
    },
    {
      name: "totalSales",
      inputs: ["getTotalFromColumn","sales"],
      value: (function(getTotalFromColumn,sales){return(
getTotalFromColumn(sales, "sales")
)})
    },
    {
      name: "totalVolume",
      inputs: ["getTotalFromColumn","sales"],
      value: (function(getTotalFromColumn,sales){return(
getTotalFromColumn(sales, "volume")
)})
    },
    {
      name: "totalsByManufacturer",
      inputs: ["manufacturers","getTotalFromColumn","salesByManufacturer","totalSales","formatStr","totalVolume"],
      value: (function(manufacturers,getTotalFromColumn,salesByManufacturer,totalSales,formatStr,totalVolume){return(
manufacturers
  .map(man => {
    const manSales = getTotalFromColumn(salesByManufacturer[man], "sales");
    const manVolume = getTotalFromColumn(salesByManufacturer[man], "volume");
    return {
      manufacturer: man,
      sales: manSales,
      salesPct: (100 * manSales) / totalSales,
      volume: formatStr(manVolume),
      volumePct: (100 * manVolume) / totalVolume,
      volume: manVolume
    };
  })
  .sort((a, b) => b.sales > a.sales)
)})
    },
    {
      inputs: ["DOM","d3","totalsByManufacturer","formatStr"],
      value: (function(DOM,d3,totalsByManufacturer,formatStr){return(
DOM.download(
  new Blob(
    [
      d3.csvFormat(
        totalsByManufacturer.map(man => {
          return {
            manufacturer: man.manufacturer,
            sales: formatStr(man.sales),
            salesPct: formatStr(man.salesPct),
            volume: formatStr(man.volume),
            volumePct: formatStr(man.volumePct)
          };
        })
      )
    ],
    { type: "text/csv" }
  ),
  "sales_and_volume_by_manufacturer.csv"
)
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`Detail: what proportion of sales correspond to multi-companies sales?`
)})
    },
    {
      name: "totalSalesVariousManufacturers",
      inputs: ["getTotalFromColumn","sales"],
      value: (function(getTotalFromColumn,sales){return(
getTotalFromColumn(
  sales.filter(sale => sale.manufacturers.length > 1),
  "sales"
)
)})
    },
    {
      name: "totalVolumeVariousManufacturers",
      inputs: ["getTotalFromColumn","sales"],
      value: (function(getTotalFromColumn,sales){return(
getTotalFromColumn(
  sales.filter(sale => sale.manufacturers.length > 1),
  "volume"
)
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md` ## Sales and volume by country`
)})
    },
    {
      name: "totalsByCountry",
      inputs: ["countries","getTotalFromColumn","salesByCountry","totalSales","totalVolume"],
      value: (function(countries,getTotalFromColumn,salesByCountry,totalSales,totalVolume){return(
countries
  .map(country => {
    const countrySales = getTotalFromColumn(salesByCountry[country], "sales");
    const countryVolume = getTotalFromColumn(salesByCountry[country], "volume");
    return {
      country: country,
      sales: countrySales,
      salesPct: (100 * countrySales) / totalSales,
      volume: countryVolume,
      volumePct: (100 * countryVolume) / totalVolume
    };
  })
  .sort((a, b) => b.sales > a.sales)
)})
    },
    {
      inputs: ["DOM","d3","totalsByCountry","formatStr"],
      value: (function(DOM,d3,totalsByCountry,formatStr){return(
DOM.download(
  new Blob(
    [
      d3.csvFormat(
        totalsByCountry.map(country => {
          return {
            country: country.country,
            sales: formatStr(country.sales),
            salesPct: formatStr(country.salesPct),
            volume: formatStr(country.volume),
            volumePct: formatStr(country.volumePct)
          };
        })
      )
    ],
    { type: "text/csv" }
  ),
  "sales_and_volume_by_country.csv"
)
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md` ## Sales and volume by country for Syngenta`
)})
    },
    {
      name: "salesByCountryForSyngenta",
      inputs: ["salesByManufacturer"],
      value: (function(salesByManufacturer){return(
salesByManufacturer["Syngenta"].reduce(
  (acc, cur) => {
    if (!(cur.country in acc)) {
      acc[cur.country] = [];
    }
    acc[cur.country].push(cur);
    return acc;
  },
  {}
)
)})
    },
    {
      name: "countriesForSyngenta",
      inputs: ["salesByCountryForSyngenta"],
      value: (function(salesByCountryForSyngenta){return(
Object.keys(salesByCountryForSyngenta)
)})
    },
    {
      name: "salesForSyngenta",
      inputs: ["totalsByManufacturer"],
      value: (function(totalsByManufacturer){return(
totalsByManufacturer.filter(
  man => man.manufacturer === "Syngenta"
)[0].sales
)})
    },
    {
      name: "volumeForSyngenta",
      inputs: ["totalsByManufacturer"],
      value: (function(totalsByManufacturer){return(
totalsByManufacturer.filter(
  man => man.manufacturer === "Syngenta"
)[0].volume
)})
    },
    {
      name: "totalsByCountryForSyngenta",
      inputs: ["countriesForSyngenta","getTotalFromColumn","salesByCountryForSyngenta","salesForSyngenta","volumeForSyngenta"],
      value: (function(countriesForSyngenta,getTotalFromColumn,salesByCountryForSyngenta,salesForSyngenta,volumeForSyngenta){return(
countriesForSyngenta
  .map(country => {
    const countrySales = getTotalFromColumn(
      salesByCountryForSyngenta[country],
      "sales"
    );
    const countryVolume = getTotalFromColumn(
      salesByCountryForSyngenta[country],
      "volume"
    );
    return {
      country: country,
      sales: countrySales,
      salesPct: (100 * countrySales) / salesForSyngenta,
      volume: countryVolume,
      volumePct: (100 * countryVolume) / volumeForSyngenta
    };
  })
  .sort((a, b) => b.sales > a.sales)
)})
    },
    {
      inputs: ["DOM","d3","totalsByCountryForSyngenta","formatStr"],
      value: (function(DOM,d3,totalsByCountryForSyngenta,formatStr){return(
DOM.download(
  new Blob(
    [
      d3.csvFormat(
        totalsByCountryForSyngenta.map(country => {
          return {
            country: country.country,
            sales: formatStr(country.sales),
            salesPct: formatStr(country.salesPct),
            volume: formatStr(country.volume),
            volumePct: formatStr(country.volumePct)
          };
        })
      )
    ],
    { type: "text/csv" }
  ),
  "sales_and_volume_by_country_for_syngenta.csv"
)
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`----

### Boring part...
`
)})
    },
    {
      name: "panCompoundsInSales",
      inputs: ["panCompounds","salesLut"],
      value: (function(panCompounds,salesLut){return(
panCompounds.filter(com => com in salesLut)
)})
    },
    {
      name: "panCompoundsNotInSales",
      inputs: ["panCompounds","salesLut"],
      value: (function(panCompounds,salesLut){return(
panCompounds.filter(com => !(com in salesLut))
)})
    },
    {
      name: "salesCompoundsInPan",
      inputs: ["salesCompounds","panLut"],
      value: (function(salesCompounds,panLut){return(
salesCompounds.filter(com => com in panLut)
)})
    },
    {
      name: "salesCompoundsNotInPan",
      inputs: ["salesCompounds","panLut"],
      value: (function(salesCompounds,panLut){return(
salesCompounds.filter(com => !(com in panLut)).sort()
)})
    },
    {
      name: "salesCompounds",
      inputs: ["salesLut"],
      value: (function(salesLut){return(
Object.keys(salesLut)
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`Reversed view of the sales: grouped by every compound of the product composition`
)})
    },
    {
      name: "salesLut",
      inputs: ["sales"],
      value: (function(sales){return(
sales.reduce((acc, cur) => {
  cur.compounds.forEach(com => {
    if (!(com in acc)) {
      acc[com] = [];
    }
    acc[com].push(cur);
  });
  return acc;
}, {})
)})
    },
    {
      inputs: ["md"],
      value: (function(md){return(
md`Samples of sales of products with 6 compounds:`
)})
    },
    {
      inputs: ["sales"],
      value: (function(sales){return(
sales.filter(row => row.compounds.length > 5)
)})
    },
    {
      name: "panCompounds",
      inputs: ["panLut"],
      value: (function(panLut){return(
Object.keys(panLut)
)})
    },
    {
      name: "panLut",
      inputs: ["pan"],
      value: (function(pan){return(
pan.reduce((acc, cur) => {
  if (!(cur.uppername in acc)) {
    acc[cur.uppername] = [];
  }
  acc[cur.uppername] = cur;
  return acc;
}, {})
)})
    },
    {
      name: "PRECISION",
      value: (function(){return(
2
)})
    },
    {
      name: "formatStr",
      inputs: ["PRECISION"],
      value: (function(PRECISION){return(
function formatStr(num) {
  return num.toFixed(PRECISION).replace(".", ",");
}
)})
    },
    {
      name: "d3",
      inputs: ["require"],
      value: (function(require){return(
require("d3")
)})
    }
  ]
};

const notebook = {
  id: "7193f10baad90950@350",
  modules: [m0]
};

export default notebook;
