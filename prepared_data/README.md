# Prepared data

- [`pan_pesticides.csv`](./pan_pesticides.csv): list of 310 pesticides in the PAN list: id, CAS code, name. Prepared on 2019-04-08, copy/paste in LibreOffice from table in [original docx file](../original_data/PAN_HHP_List_2019.docx).
